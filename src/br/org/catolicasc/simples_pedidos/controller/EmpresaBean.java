package br.org.catolicasc.simples_pedidos.controller;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.org.catolicasc.simples_pedidos.dao.EmpresaDao;
import br.org.catolicasc.simples_pedidos.entity.Empresa;

@ManagedBean
@ViewScoped
public class EmpresaBean {

	private EmpresaDao empresaDao = new EmpresaDao();
	private Empresa empresa = new Empresa();

	public void salva(Empresa empresa){
		empresaDao.salva(empresa);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Empresa salvo"));
		this.empresa = new Empresa();
	}
	
	public Empresa getEmpresa(){
		return this.empresa;
	}
	
    public List<Empresa> getEmpresas(){
    	return empresaDao.listaTodos();
    }
	
	public void excluir(Empresa empresa){
		this.empresaDao.remove(empresa);
	}

	public void editar(Empresa empresa){
		this.empresa = empresa;
	}
	
	public void criar(){
		this.empresa = new Empresa();
	}
	
}
