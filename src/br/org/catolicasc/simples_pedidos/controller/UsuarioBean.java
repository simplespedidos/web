package br.org.catolicasc.simples_pedidos.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.org.catolicasc.simples_pedidos.dao.UsuarioDao;
import br.org.catolicasc.simples_pedidos.entity.Usuario;

@ManagedBean
@ViewScoped
public class UsuarioBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7058051429682573225L;
	
	private UsuarioDao usuarioDao = new UsuarioDao();
	private Usuario usuario = new Usuario();
	
	public void salva(Usuario usuario){
		usuarioDao.salva(usuario);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Usuário cadastrado com sucesso!"));
		this.usuario = new Usuario();
	}
	
	public Usuario getUsuario(){
		return usuario;
	}
	
    public List<Usuario> getUsuarios(){
    	return this.usuarioDao.listaTodos();
    }
	
	public void excluir(Usuario usuario){
		this.usuarioDao.remove(usuario);
	}

	public void editar(Usuario usuario){
		this.usuario = usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}
