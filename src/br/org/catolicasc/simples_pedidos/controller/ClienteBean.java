package br.org.catolicasc.simples_pedidos.controller;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.org.catolicasc.simples_pedidos.dao.ClienteDao;
import br.org.catolicasc.simples_pedidos.entity.Cliente;

@ManagedBean
@ViewScoped
public class ClienteBean {
	
	private ClienteDao clienteDao = new ClienteDao();
	private Cliente cliente = new Cliente();

	public void salva(Cliente cliente){
		clienteDao.salva(cliente);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Cliente salvo"));
		this.cliente = new Cliente();
	}
	
	public void editar(Cliente cliente) {
		this.setCliente(cliente);
	}
	
	public void criar() {
		this.cliente = new Cliente();
	}
	
	
	public Cliente getCliente(){
		return this.cliente;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<Cliente> getClientes(){
		return this.clienteDao.listaTodos();
	}
	
	public void excluir(Cliente cliente){
		this.clienteDao.remove(cliente);
	}
}
