package br.org.catolicasc.simples_pedidos.controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.org.catolicasc.simples_pedidos.dao.ItemPedidoDao;
import br.org.catolicasc.simples_pedidos.entity.ItemPedido;
import br.org.catolicasc.simples_pedidos.entity.Pedido;

@ManagedBean
@ViewScoped
public class ItemPedidoBean {
	private ItemPedido itemPedido = new ItemPedido();
	private ItemPedidoDao itemPedidoDao = new ItemPedidoDao();

	public ItemPedido getItemPedido() {
		return this.itemPedido;
	}

	public List<ItemPedido> getItensPedido() {
		return this.itemPedidoDao.listaTodos();
	}
	
	public void criar() {
		this.itemPedido = new ItemPedido();
	}

	public void setItemPedido(ItemPedido itemPedido) {
		this.itemPedido = itemPedido;
	}

	public void editar(ItemPedido itemPedido) {
		this.setItemPedido(itemPedido);
	}

	public void salva(ItemPedido itemPedido) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("ItemPedido salvo"));
		itemPedidoDao.salva(itemPedido);
	}

}
