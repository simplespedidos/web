package br.org.catolicasc.simples_pedidos.controller;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import br.org.catolicasc.simples_pedidos.dao.UsuarioDao;
import br.org.catolicasc.simples_pedidos.entity.Usuario;

@SessionScoped
@ManagedBean
public class LoginBean {

	UsuarioDao dao = new UsuarioDao();
	private Usuario usuario = new Usuario();

	public String efetuaLogin() {
		UsuarioDao dao = new UsuarioDao();
		if (dao.existe(this.usuario) != null){
			usuario = dao.existe(this.usuario);
			return "empresa/listar";
		} else {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Usuario incorreto"));    
			this.usuario = new Usuario();
			return "login";
		}
	}
	
	public String efetuaCadastro() {
		dao.salva(usuario);
		return "empresa/listar?faces-redirect=true";
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public boolean isLogado() {
		return usuario != null && usuario.getId() != null;
	}
	
	public String logout() {
		this.usuario = new Usuario();
		return "/login";
	}
	
	public void setUsuario(Usuario usuario){
		this.usuario = usuario;
	}
	
	public String irEmpresa(){
		return "../empresa/listar.xhtml?faces-redirect=true";
	}

}
