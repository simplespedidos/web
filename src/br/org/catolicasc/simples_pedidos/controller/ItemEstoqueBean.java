package br.org.catolicasc.simples_pedidos.controller;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.org.catolicasc.simples_pedidos.dao.ItemEstoqueDao;
import br.org.catolicasc.simples_pedidos.entity.ItemEstoque;

@ManagedBean
@ViewScoped
public class ItemEstoqueBean {

	private ItemEstoqueDao itemEstoqueDao = new ItemEstoqueDao();
	private ItemEstoque itemEstoque = new ItemEstoque();

	public void salva(ItemEstoque itemEstoque) {
		itemEstoqueDao.salva(itemEstoque);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("itemEstoque salvo"));
		this.itemEstoque = new ItemEstoque();
	}

	public void editar(ItemEstoque itemEstoque) {
		this.itemEstoque = itemEstoque;
	}

	public ItemEstoque getItemEstoque() {
		return this.itemEstoque;
	}

	public void excluir(ItemEstoque estoque) {
		this.itemEstoqueDao.remove(estoque);
	}

	public List<ItemEstoque> getItensEstoques() {
		return itemEstoqueDao.listaTodos();
	}

	public void criar() {
		itemEstoque = new ItemEstoque();
	}
}
