package br.org.catolicasc.simples_pedidos.controller;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.org.catolicasc.simples_pedidos.dao.EstoqueDao;
import br.org.catolicasc.simples_pedidos.entity.Estoque;

@ManagedBean
@ViewScoped
public class EstoqueBean {
	
	private EstoqueDao estoqueDao = new EstoqueDao();
	private Estoque estoque = new Estoque();
	
	public void salva(Estoque estoque){
		estoqueDao.salva(estoque);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Estoque salvo"));
		this.estoque = new Estoque();
	}
	
	public void editar(Estoque estoque) {
		this.estoque = estoque;
	}
	
	public void criar(){
		this.estoque = new Estoque();
	}
	
	public Estoque getEstoque(){
		return this.estoque;
	}
	
	public List<Estoque> getEstoques(){
		return estoqueDao.listaTodos();
	}
	
	public void excluir(Estoque estoque){
		this.estoqueDao.remove(estoque);
	}
}
