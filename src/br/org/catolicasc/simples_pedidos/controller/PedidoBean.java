package br.org.catolicasc.simples_pedidos.controller;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.org.catolicasc.simples_pedidos.dao.PedidoDao;
import br.org.catolicasc.simples_pedidos.entity.Pedido;

@ManagedBean
@ViewScoped
public class PedidoBean {
	private PedidoDao pedidoDao = new PedidoDao();
	private Pedido pedido = new Pedido();

	public Pedido getPedido() {
		return this.pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public List<Pedido> getPedidos() {
		return this.pedidoDao.listaTodos();
	}

	public void criar() {
		this.pedido = new Pedido();
	}

	public void editar(Pedido pedido) {
		this.setPedido(pedido);
	}

	public void salva(Pedido pedido) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Pedido salvo"));
		pedidoDao.salva(pedido);
	}

}
