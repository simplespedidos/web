package br.org.catolicasc.simples_pedidos.controller;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import br.org.catolicasc.simples_pedidos.dao.ProdutoDao;
import br.org.catolicasc.simples_pedidos.entity.Produto;

@ManagedBean
@ViewScoped
public class ProdutoBean {

	private ProdutoDao produtoDao = new ProdutoDao();
	private Produto produto = new Produto();

	public void salva(Produto produto) {
		produtoDao.salva(produto);
		this.produto = new Produto();
	}

	public void editar(Produto produto) {
		this.produto = produto;
	}

	public Produto getProduto() {
		return this.produto;
	}

	public List<Produto> getProdutos() {
		return this.produtoDao.listaTodos();
	}

	public void excluir(Produto produto) {
		this.produtoDao.remove(produto);
	}

	public void criar() {
		this.produto = new Produto();
	}
}
